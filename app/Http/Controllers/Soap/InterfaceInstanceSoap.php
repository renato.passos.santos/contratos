<?php


namespace App\Http\Controllers\Soap;


interface InterfaceInstanceSoap
{
    public static function init();

    public static function montaCabecalho();
}
